**Example Project for Nucleo STM32F767ZI running 4DIAC Forte**

* Cmake build system
* Native FreeRTOS 10.3.1 without CMSIS wrapper.
* More stable ZeroCopy ethernet driver from FreeRTOS+TCP
* 4DIAC Forte 1.12.0
* Precompiled binary in the bin directory:
    IP: 192.168.0.157 is set in the main.cpp
* HW init code created with CubeMX
* FreeRTOS and Forte added as submodules
    Use: git clone --recurse-submodules to get all sources
* In the Utils directory is a forte.patch file. Will be applied to forte sources by Cmake
* The patch adds support for FreeRTOS+TCP stack in forte and makes it possible to build forte as subproject.
    Only TCP, no UDP/IGMP for now.

**Memory usage**
* Running at 216MHz
* 400kB heap allocated
* 52kB used with a running Blinky example in Forte and Monitoring with live value watch in 4DIAC IDE

**How to build?**
* Tested only on Linux/Debian
* Set TOOLCHAIN_PREFIX in the main CMakeLists.txt to GNU ARM Embedded toolchain bin directory

* cd ClonedSRC_DIR
* mkdir build
* cd build
* cmake .
* make
* flash to youre device or uncomment the post build command in the bottom of the main CMakeLists.txt. If OpenOCD is installed it will flash automaticly
**This sources are only for the Nucleo STM32F767ZI**

**This is just an example Project.**

**THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.**
