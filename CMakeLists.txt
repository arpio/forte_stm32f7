cmake_minimum_required(VERSION 3.13)
set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_VERSION 1)

SET(TOOLCHAIN_PREFIX)
SET(CMAKE_BUILD_TYPE "Relaese")
SET(CMAKE_C_COMPILER_WORKS 1)
SET(CMAKE_C_COMPILER ${TOOLCHAIN_PREFIX}/arm-none-eabi-gcc)
SET(CMAKE_CXX_COMPILER_WORKS 1)
SET(CMAKE_CXX_COMPILER ${TOOLCHAIN_PREFIX}/arm-none-eabi-g++)
set(CMAKE_ASM_COMPILER  ${TOOLCHAIN_PREFIX}/arm-none-eabi-gcc)
set(CMAKE_AR ${TOOLCHAIN_PREFIX}/arm-none-eabi-ar)
set(CMAKE_OBJCOPY ${TOOLCHAIN_PREFIX}/arm-none-eabi-objcopy)
set(CMAKE_OBJDUMP ${TOOLCHAIN_PREFIX}/arm-none-eabi-objdump)
set(SIZE ${TOOLCHAIN_PREFIX}/arm-none-eabi-size)

set(CHIP STM32F767xx)
add_definitions(-DUSE_HAL_DRIVER -D${CHIP})

set(FPU_FLAGS
    "--specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -Wl,--start-group -lc -lm -Wl,--end-group")

set(COMMON_FLAGS
    "-mcpu=cortex-m7 ${FPU_FLAGS} -O2 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF")
    
set(CMAKE_CXX_FLAGS_INIT "${COMMON_FLAGS} -std=c++11 -fno-threadsafe-statics -fno-rtti -fno-exceptions")
set(CMAKE_C_FLAGS_INIT "${COMMON_FLAGS} -std=gnu11")

set(LINKER_SCRIPT ${CMAKE_SOURCE_DIR}/Firmware/STM32HW/STM32F767ZITx_FLASH.ld)

set(CMAKE_EXE_LINKER_FLAGS_INIT "-Wl,-gc-sections,--print-memory-usage -T ${LINKER_SCRIPT}")

set(CMAKE_ASM_FLAGS
    "-mcpu=cortex-m7 -g3 -c -x assembler-with-cpp --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb") 

PROJECT(Forte_STM32F7 C CXX ASM)

set(CMAKE_VERBOSE_MAKEFILE ON)
set(CMAKE_CXX_STANDARD 11)

add_subdirectory(ThirdParty)

file(GLOB STM32HW_SRC   "Firmware/STM32HW/Core/Src/*.*"
                        "Firmware/STM32HW/Drivers/STM32F7xx_HAL_Driver/Src/*.*"
                        "Firmware/STM32HW/*.s")
                        
file(GLOB FIRMWARE_SRC  "Firmware/src/*.*")

add_executable(${PROJECT_NAME}.elf ${FIRMWARE_SRC} ${STM32HW_SRC})

set_target_properties(${PROJECT_NAME}.elf PROPERTIES LINKER_LANGUAGE CXX)

target_include_directories( ${PROJECT_NAME}.elf PUBLIC  ${PROJECT_SOURCE_DIR}/Firmware/STM32HW/Core/Inc
                            ${PROJECT_NAME}.elf PUBLIC  ${PROJECT_SOURCE_DIR}/Firmware/STM32HW/Drivers/STM32F7xx_HAL_Driver/Inc
                            ${PROJECT_NAME}.elf PUBLIC  ${PROJECT_SOURCE_DIR}/Firmware/STM32HW/Drivers/CMSIS/Device/ST/STM32F7xx/Include
                            ${PROJECT_NAME}.elf PUBLIC  ${PROJECT_SOURCE_DIR}/Firmware/STM32HW/Drivers/CMSIS/Include
                            ${PROJECT_NAME}.elf PUBLIC  ${PROJECT_SOURCE_DIR}/Firmware/inc
                            ${PROJECT_NAME}.elf PUBLIC  ${PROJECT_SOURCE_DIR}/ThirdParty/forte/src/arch/freeRTOS)

add_dependencies(${PROJECT_NAME}.elf fortePatchCreate)

target_link_libraries(${PROJECT_NAME}.elf ${CMAKE_BINARY_DIR}/ThirdParty/forte/src/libforte-static.a FreeRTOS)

set(CMAKE_EXE_LINKER_FLAGS
    "${CMAKE_EXE_LINKER_FLAGS} --specs=nosys.specs -Wl,-Map=${PROJECT_BINARY_DIR}/${PROJECT_NAME}.map")

#add_custom_command(TARGET ${PROJECT_NAME}.elf POST_BUILD
#        COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_BINARY_DIR}/${PROJECT_NAME}.elf ${CMAKE_CURRENT_SOURCE_DIR}/bin/${PROJECT_NAME}.elf
#        COMMAND openocd  -f Utils/f767zi.cfg  -c "program ${CMAKE_BINARY_DIR}/${PROJECT_NAME}.elf verify reset exit"
#        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
#        COMMENT "Flashing ${PROJECT_NAME}.elf to ${CHIP}")
